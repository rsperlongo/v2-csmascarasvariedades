const express = require('express');
const app = express();
const router = express.Router();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const Products = require('../models/Products');

server.listen(4000);

io.on('connection', function (socket) {
  socket.on('updatedata', function (data) {
    io.emit('update-data', { data: data });
  });
});

// list data
router.get('/', function (req, res) {
  Products.find(function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

// item sales report
router.get('/itemsales', function (req, res, next) {
  Products.aggregate(
    [
      {
        $group: {
          _id: { itemId: '$itemId', itemName: '$itemName' },
          totalPrice: {
            $sum: '$totalPrice',
          },
        },
      },
      { $sort: { totalPrice: 1 } },
    ],
    function (err, products) {
      if (err) return next(err);
      res.json(products);
    }
  );
});

// get data by id
router.get('/:id', function (req, res, next) {
  Products.findById(req.params.id, function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

// post data
router.post('/', function (req, res, next) {
  Products.create(req.body, function (err, products) {
    if (err) {
      console.log(err);
      return next(err);
    }
    res.json(products);
  });
});

// put data
router.put('/:id', function (req, res, next) {
  Products.findByIdAndUpdate(req.params.id, req.body, function (err, products) {
    if (err) {
      console.log(err);
      return next(err);
    }
    res.json(products);
  });
});

// delete data by id
router.delete('/:id', function (req, res, next) {
  Products.findByIdAndRemove(req.params.id, req.body, function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

module.exports = router;
