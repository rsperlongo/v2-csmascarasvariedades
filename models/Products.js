const mongoose = require('mongoose');

const ProductsSchema = new mongoose.Schema({
    id: String,
    itemId: String,
    itemName: String,
    category:   String,
    quantity:   String,
    price:  String,
    totalprice: String,
    description: String,
});

module.exports = mongoose.model('Products', ProductsSchema);